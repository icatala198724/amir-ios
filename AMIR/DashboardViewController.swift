//
//  DashboardViewController.swift
//  AMIR
//
//  Created by Ismael Catala on 3/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreBluetooth

class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state) {
        case .poweredOn:
            print("bluetooth Powered ON")
        case .poweredOff:
            print("bluetooth Powered OFF")
            let alertaGuia = UIAlertController(title: title, message: "Se necesita activar el bluetooth", preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
                let url = URL(string: "App-Prefs:root=Bluetooth") //for bluetooth setting
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            })
            alertaGuia.addAction(aceptar)
            present(alertaGuia, animated: true, completion: nil)
            
            
            
            
            //app.openURL(url!)
        case .unsupported:
            print("bltyh noit supported")
        case .unknown:
            print("bltyh noit unknown")
        case .resetting:
            print("bltyh noit resetting")
        case .unauthorized:
            print("bltyh noit unauthorized")
        }
    }
    
    
    var manager: CBCentralManager!
    @IBOutlet weak var demoView: UIView!
    var misObjetos: [Clase] = []
    var tokenAccesoUsuario : String = ""
    @IBOutlet weak var TodayLabel: UILabel!
    @IBOutlet weak var AllLabel: UILabel!
    @IBOutlet weak var SwitchClases: UISwitch!
    let colorBlack = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
    let colorMorado = UIColor(red:0.63, green:0.04, blue:0.43, alpha:1.0)
    @IBAction func SwitchClasesPressed(_ sender: Any) {
        if SwitchClases.isOn{
            TodayLabel.textColor = colorMorado
            AllLabel.textColor = colorBlack
            print(misObjetos.count)
            misObjetos.removeAll()
            print(misObjetos.count)
            peticionTodayClases()
        }else{
            TodayLabel.textColor = colorBlack
            AllLabel.textColor = colorMorado
            print(misObjetos.count)
             misObjetos.removeAll()
            print(misObjetos.count)
            peticionAllClases()
        }
    }
    let defaults:UserDefaults = UserDefaults.standard
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var miTablaColeccion: UITableView!
    
    @IBOutlet weak var totalClasesLabel: UILabel!
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return misObjetos.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let identifier = "Item"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ClaseCellTableViewCell
        cell.clasesLabel.text = misObjetos[indexPath.row].flavorCode
        //cell.ClaseLabel.text = misObjetos[indexPath.row].flavorCode
        //cell.AulaLabel.text = misObjetos[indexPath.row].sala
        cell.dateLabel.text = misObjetos[indexPath.row].startDate
        cell.codigoLabel.text = misObjetos[indexPath.row].sala
        
        //cell.itemImage.image = UIImage.init(imageLiteralResourceName: tvSeries[indexPath.row])
        
        return cell
    }
    
    
    
   
    
    
    
    /*
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return misObjetos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "Item"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! ClasesCollectionViewCell
        cell.ClaseLabel.text = misObjetos[indexPath.row].flavorCode
        cell.AulaLabel.text = misObjetos[indexPath.row].sala
        cell.HoraLabel.text = misObjetos[indexPath.row].startDate
        
        //cell.itemImage.image = UIImage.init(imageLiteralResourceName: tvSeries[indexPath.row])
        
        return cell
    }
    */

    @IBAction func SettingsButtonPressed(_ sender: Any) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager(delegate: self, queue: nil)
        // Styles
        demoView.layer.cornerRadius = 0
        demoView.layer.shadowColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
        demoView.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        demoView.layer.shadowRadius = 0
        demoView.layer.shadowOpacity = 0.45
        
        
        
        
        /*let clase1 = Clase(flavorCode: "Cardiología",eventId:"1"  , startDate:"14:15 - 15:15",endDate:"", beacon: "Aula - 1")
        let clase2 = Clase(flavorCode: "Dermatología",eventId:"1", startDate:"15:25 - 16:15",endDate:"", beacon: "Aula - 2")
        let clase3 = Clase(flavorCode: "Intestino",eventId:"1", startDate:"17:25 - 18:15",endDate:"", beacon: "Aula - 3")
        
        misObjetos.append(clase1)
        misObjetos.append(clase2)
        misObjetos.append(clase3)*/
        peticionTodayClases()
        let token = self.defaults.string(forKey: "tokenUser" )!
        let headers = ["Authorization": "Bearer \(token)",
            ]
        
        Alamofire.request("http://login.academiamir.com/amir-central/api/account/userinfo", method: .get, headers: headers).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print(swiftyJsonVar[])
               /* print(swiftyJsonVar[0]["user"]["name"])
                print(swiftyJsonVar[0]["user"]["surname"])
                print(swiftyJsonVar[0]["user"]["email"])*/
                
                
            }
        }
        
        
        

        // Do any additional setup after loading the view.
    }
    func peticionAllClases(){
        let sv = UIViewController.displaySpinner(onView: self.view)

        let token = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
        
        let headers = ["Authorization": "Basic \(token)",
            "Content-Type": "application/x-www-form-urlencoded"]
        /*let parameters = [
         "grant_type": "password",
         "username": usuario,
         "password": contra
         ]*/
        let parameters = [
            "grant_type": "password",
            "username": self.defaults.string(forKey: "usuario" )!,
            "password": self.defaults.string(forKey: "password" )!
        ]
        Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print(swiftyJsonVar)
                //print(swiftyJsonVar["access_token"])
                if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                    self.tokenAccesoUsuario = tokenDeAcceso
                    
                    self.defaults.set(tokenDeAcceso, forKey: "tokenUser")
                    self.defaults.set(true, forKey: "HasAppBeenOpenedBefore")
                    //print (tokenDeAcceso)
                    //print(self.tokenAccesoUsuario)
                    let headers = ["Authorization": "Bearer \(self.tokenAccesoUsuario)",
                    ]
                    
                    Alamofire.request("http://login.academiamir.com/amir-central/api/events/next", method: .get, headers: headers).responseJSON { (responseData) -> Void in
                        if((responseData.result.value) != nil) {
                            let swiftyJsonVar = JSON(responseData.result.value!)
                            //print(swiftyJsonVar)
                            //print(swiftyJsonVar.count)
                            self.totalClasesLabel.text = String(swiftyJsonVar.count) + " clases"
                            /*print(swiftyJsonVar[0]["user"]["surname"])
                            print(swiftyJsonVar[0]["user"]["email"])*/
                            for i in 0..<swiftyJsonVar.count{
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "HH:mm dd-MM-yyyy"
                                var fechaInicio : String = ""
                                if let date = dateFormatterGet.date(from: swiftyJsonVar[i]["startDate"].string!){
                                    //print(dateFormatterPrint.string(from: date))
                                    fechaInicio = dateFormatterPrint.string(from: date)
                                }
                                else {
                                    print("There was an error decoding the string")
                                }
                                let clase10 = Clase(flavorCode: swiftyJsonVar[i]["flavorCode"].string!,eventId:swiftyJsonVar[i]["eventId"].int!, startDate:fechaInicio,endDate:swiftyJsonVar[i]["endDate"].string!, beacon: swiftyJsonVar[i]["beacon"].string!, sala: swiftyJsonVar[i]["classroom"]["code"].string!, latitude: swiftyJsonVar[i]["classroom"]["latitude"].double!, longitude: swiftyJsonVar[i]["classroom"]["longitude"].double!, fullAddress: swiftyJsonVar[i]["classroom"]["fullAddress"].string!)
                                self.misObjetos.append(clase10)
                               
                                
                                
                                
                                self.miTablaColeccion.reloadData()
                                
                            }
                            
                        }
                    }
                    //self.miColeccion.reloadData()
                    UIViewController.removeSpinner(spinner: sv)
                    
                }
                else{
                    print("ERROR")
                }
                //self.miColeccion.reloadData()
            }
            //self.miColeccion.reloadData()
        }
        //self.miColeccion.reloadData()
        
    }
    func peticionTodayClases(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        let token = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
        
        let headers = ["Authorization": "Basic \(token)",
            "Content-Type": "application/x-www-form-urlencoded"]
        /*let parameters = [
         "grant_type": "password",
         "username": usuario,
         "password": contra
         ]*/
        let parameters = [
            "grant_type": "password",
            "username": self.defaults.string(forKey: "usuario" )!,
            "password": self.defaults.string(forKey: "password" )!
        ]
        Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                //print(swiftyJsonVar["access_token"])
                if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                    self.tokenAccesoUsuario = tokenDeAcceso
                    
                    self.defaults.set(tokenDeAcceso, forKey: "tokenUser")
                    self.defaults.set(true, forKey: "HasAppBeenOpenedBefore")
                    print (tokenDeAcceso)
                    print(self.tokenAccesoUsuario)
                    let headers = ["Authorization": "Bearer \(self.tokenAccesoUsuario)",
                    ]
                    
                    Alamofire.request("http://login.academiamir.com/amir-central/api/events/today", method: .get, headers: headers).responseJSON { (responseData) -> Void in
                        if((responseData.result.value) != nil) {
                            let swiftyJsonVar = JSON(responseData.result.value!)
                            //print(swiftyJsonVar)
                            print(swiftyJsonVar.count)
                            self.totalClasesLabel.text = String(swiftyJsonVar.count) + " clases"
                            /*print(swiftyJsonVar[0]["user"]["surname"])
                             print(swiftyJsonVar[0]["user"]["email"])*/
                            for i in 0..<swiftyJsonVar.count{
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "HH:mm dd-MM-yyyy"
                                var fechaInicio : String = ""
                                if let date = dateFormatterGet.date(from: swiftyJsonVar[i]["startDate"].string!){
                                    print(dateFormatterPrint.string(from: date))
                                    fechaInicio = dateFormatterPrint.string(from: date)
                                    //print("Fecha de comprobar:"+fechaInicio)
                                    let secondDate = Date()
                                    let calendar = Calendar.current
                                    let firstDate = calendar.date(byAdding: .minute, value: 20, to: date)
                                    var mifecha : Date
                                    mifecha = firstDate!
                                    print(firstDate!)
                                    print(secondDate)
                                    //self.compararFechas(fechaInicio: date, fechaFin: firstDate!)
                                    
                                    
                                    
                                    
                                    
                                }
                                else {
                                    print("There was an error decoding the string")
                                }
                                //print("JSON: "+swiftyJsonVar[i]["classroom"].string!)
                                let clase10 = Clase(flavorCode: swiftyJsonVar[i]["flavorCode"].string!,eventId:swiftyJsonVar[i]["eventId"].int!, startDate:fechaInicio,endDate:swiftyJsonVar[i]["endDate"].string!, beacon: swiftyJsonVar[i]["beacon"].string!, sala: swiftyJsonVar[i]["classroom"]["code"].string!, latitude: swiftyJsonVar[i]["classroom"]["latitude"].double!, longitude: swiftyJsonVar[i]["classroom"]["longitude"].double!, fullAddress: swiftyJsonVar[i]["classroom"]["fullAddress"].string!)
                                self.misObjetos.append(clase10)
                                
                                
                                
                                
                                self.miTablaColeccion.reloadData()
                                
                            }
                            
                        }
                    }
                    //self.miColeccion.reloadData()
                    self.miTablaColeccion.reloadData()
                    UIViewController.removeSpinner(spinner: sv)
                    
                }
                else{
                    print("ERROR")
                }
                //self.miColeccion.reloadData()self.miTablaColeccion.reloadData()
                self.miTablaColeccion.reloadData()
            }
            //self.miColeccion.reloadData()
            self.miTablaColeccion.reloadData()
        }
        //self.miColeccion.reloadData()
        self.miTablaColeccion.reloadData()
        
    }
    func compararFechas(fechaInicio: Date, fechaFin: Date){
        //comparamos fechas
        let secondDate = Date()
        if (secondDate >= fechaInicio ) {
            if(secondDate <= fechaFin){
                mostrarAlerta(title: "Proceso", message: "En hora")
            }else{
                mostrarAlerta(title: "Proceso", message: "Aun no hay clases")
            }
            
            
        }else{
            mostrarAlerta(title: "Proceso", message: "Aun no hay clases")
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func mostrarAlerta(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
            
        })
        alertaGuia.addAction(aceptar)
        present(alertaGuia, animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier=="segueForClase"){
            if let indexPath = self.miTablaColeccion.indexPathForSelectedRow{
                let selectedHistoria = self.misObjetos[indexPath.row]
                let destinationViewController = segue.destination as! PasandoListaViewController
                destinationViewController.clase = selectedHistoria as Clase
            }
        }
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
