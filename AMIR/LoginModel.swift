//
//  LoginModel.swift
//  AMIR
//
//  Created by Ismael Catala on 2/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import Foundation
struct LoginModel {
    
    var access_token: String
    var token_type: String
    var token_refresh: String
    
    
}
