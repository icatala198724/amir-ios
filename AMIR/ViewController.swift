//
//  ViewController.swift
//  AMIR
//
//  Created by Ismael Catala on 2/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EstimoteProximitySDK

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
class ViewController: UIViewController {
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var password: UITextField!
    let defaults:UserDefaults = UserDefaults.standard
    

    
    @IBAction func AccederButton(_ sender: Any) {
        if (self.login.text != "" && self.password.text != ""){
            var usuario : String = ""
            var contra : String = ""
            contra = password.text!
            
            usuario = login.text!
            
            let sv = UIViewController.displaySpinner(onView: self.view)
            
            let token = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
            
            let headers = ["Authorization": "Basic \(token)",
                "Content-Type": "application/x-www-form-urlencoded"]
            let parameters = [
                "grant_type": "password",
                "username": usuario,
                "password": contra
            ]
            /*let parameters = [
                "grant_type": "password",
                "username": "ismael.catalagil@ioon-technologies.com",
                "password": "Alumno01"
            ]*/
            Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    print(swiftyJsonVar)
                    //print(swiftyJsonVar["access_token"])
                    if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                        self.token = tokenDeAcceso
                        
                        self.defaults.set(tokenDeAcceso, forKey: "tokenUser")
                        self.defaults.set(usuario, forKey: "usuario")
                        self.defaults.set(contra, forKey: "password")
                        self.defaults.set(true, forKey: "HasAppBeenOpenedBefore")
                        print (tokenDeAcceso)
                        print(self.token)
                        
                        UIViewController.removeSpinner(spinner: sv)
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "GuiaViewController") as! GuiaViewController
                        UIApplication.shared.keyWindow?.rootViewController = viewController
                        
                    }
                    else{
                        UIViewController.removeSpinner(spinner: sv)
                        self.mostrarAlerta(title: "Error de acceso", message: "Usuario o contraseña incorrectos")
                    }
                    
                }
            }
        }else{
            mostrarAlerta(title: "Error de datos", message: "Se necesitan el usuario y la contraseña")
        }
        
        
        print("Acceder pressed")
    }
    
    
    var acceso : LoginModel!
    var token : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action:     #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        //sendPOSTRequest()
        
        
        
        
        
        
        
        print(token)
        // Do any additional setup after loading the view, typically from a nib.
    }
    @objc func tapGestureHandler() {
        login.endEditing(true)
        password.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*func sendPOSTRequest() {
        let token = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
        
        let headers = ["Authorization": "Basic \(token)",
            "Content-Type": "application/x-www-form-urlencoded"]
        let parameters = [
            "grant_type": "password",
            "username": "ivan.olivera@academiamir.com",
            "password":"alonso"
        ]
        Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                //print(swiftyJsonVar["access_token"])
                if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                    self.token = tokenDeAcceso
                    
                    print (tokenDeAcceso)
                    print(self.token)
                    
                }
                
            }
        }
        
    
        
        
        /*let request = Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON
        { response in
            debugPrint(response)
        }*/
        //print(request.response)
    }*/
    
    
    func mostrarAlerta(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
            
        })
        alertaGuia.addAction(aceptar)
         present(alertaGuia, animated: true, completion: nil)
        
    }


}

