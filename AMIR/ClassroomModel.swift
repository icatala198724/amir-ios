//
//  ClasesModel.swift
//  AMIR
//
//  Created by Ismael Catala on 3/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import Foundation
class Classroom: NSObject {
    var code: String?
    var desc: String?
    var fullAddress: String?
    var zone: String?
    var latitude: Double?
    var longitude: Double?
    init(code: String, desc: String, fullAddress: String, zone: String, latitude: Double, longitude: Double) {
        self.code = code
        self.desc = desc
        self.fullAddress = fullAddress
        self.zone = zone
        
        self.latitude = latitude
        self.longitude = longitude
        super.init()
    }
}
