//
//  PasandoListaViewController.swift
//  AMIR
//
//  Created by Ismael Catalá on 19/9/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit
import EstimoteProximitySDK
import SwiftyJSON
import MapKit
import CoreLocation
import Alamofire
class PasandoListaViewController: UIViewController {
    var clase : Clase!
    var proximityObserver: ProximityObserver!
    var timerIsOn = false
    var contadorMenos : Int = 15
    @IBOutlet weak var vistaBeacons: UIView!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var label2Info: UILabel!
    var timer = Timer()
    let defaults:UserDefaults = UserDefaults.standard
    @IBAction func volverButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    var tokenAccesoUsuario : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        vistaBeacons.isHidden = false
}
        
    
        
    var contador : Int = 0
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func compararFechas(fechaInicio: Date, fechaFin: Date) -> Bool{
        //comparamos fechas
        let secondDate = Date()
        if (secondDate >= fechaInicio ) {
            if(secondDate <= fechaFin){
                //mostrarAlerta(title: "Proceso", message: "En hora")
                return true
            }else{
                //mostrarAlerta(title: "Proceso", message: "Aun no hay clases")
                return false
            }
            
            
        }else{
            //mostrarAlerta(title: "Proceso", message: "Aun no hay clases")
            return false
        }
    }
    func mostrarAlerta(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
            
        })
        alertaGuia.addAction(aceptar)
        present(alertaGuia, animated: true, completion: nil)
        
    }
    func mostrarAlertaAulaEncontrada(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
            self.dismiss(animated: true, completion: nil)
        })
        alertaGuia.addAction(aceptar)
        present(alertaGuia, animated: true, completion: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        print(clase.sala!)
        print(clase.beacon!)
        print(clase.eventId!)
        print(clase.startDate!)
        // Do any additional setup after loading the view.
        var enHora : Bool = false
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "HH:mm dd-MM-yyyy"
        var presente : Bool = false
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm dd-MM-yyyy"
        var fechaInicio : String = ""
        if let date = dateFormatterGet.date(from: clase.startDate!){
            print(dateFormatterPrint.string(from: date))
            fechaInicio = dateFormatterPrint.string(from: date)
            //print("Fecha de comprobar:"+fechaInicio)
            let secondDate = Date()
            let calendar = Calendar.current
            let firstDate = calendar.date(byAdding: .minute, value: 20, to: date)
            var mifecha : Date
            mifecha = firstDate!
            print(firstDate!)
            print(secondDate)
            if self.compararFechas(fechaInicio: date, fechaFin: firstDate!){
                enHora = true
            }
            
        }
        else {
            print("There was an error decoding the string")
        }
        
            //ESTIMOTES
            // Override point for customization after application launch.
            print("pidiendo credentials")
            let cloudCredentials = CloudCredentials(appID: "test-proximity-ha3",
                                                    appToken: "ce495d748e812ca544c5d8c3af3e7f03")
            
            
            print("Creando Observador")
            
            // Create observer instance
            self.proximityObserver = ProximityObserver(credentials: cloudCredentials, onError: { error in
                print("Ooops! \(error)")
            })
            // Define zones
        let blueberryZone = ProximityZone(tag: "aula", range: ProximityRange.custom(desiredMeanTriggerDistance: 10.00 )!)
        
            blueberryZone.onEnter = { zoneContext in
                print("Entered near range of tag 'AULA'. Attachments payload: \(zoneContext.attachments)")
                let beaconSala = zoneContext.attachments["beacon"]
                
                if beaconSala != nil && beaconSala == self.clase.beacon{
                presente = true
                    
                    
                    
            
                    
                    if enHora && presente{
                        print("En clase fisica")
                        self.labelInfo.text = "Aula encontrada"
                        
                        let token = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
                        
                        let headers = ["Authorization": "Basic \(token)",
                            "Content-Type": "application/x-www-form-urlencoded"]
                        /*let parameters = [
                         "grant_type": "password",
                         "username": usuario,
                         "password": contra
                         ]*/
                        let parameters = [
                            "grant_type": "password",
                            "username": self.defaults.string(forKey: "usuario" )!,
                            "password": self.defaults.string(forKey: "password" )!
                        ]
                        Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
                            if((responseData.result.value) != nil) {
                                let swiftyJsonVar = JSON(responseData.result.value!)
                                print(swiftyJsonVar)
                                //print(swiftyJsonVar["access_token"])
                                if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                                    self.tokenAccesoUsuario = tokenDeAcceso
                                    
                                    self.defaults.set(tokenDeAcceso, forKey: "tokenUser")
                                    self.defaults.set(true, forKey: "HasAppBeenOpenedBefore")
                                    print (tokenDeAcceso)
                                    print(self.tokenAccesoUsuario)
                                    
                                    
                                   
                                    
                                    let headers = ["content-type": "application/json",
                                                   "Authorization": "Bearer \(self.tokenAccesoUsuario)"]
                                   
                                    let financials = [
                                        
                                            "confirmTime": nil,
                                            "classCode": nil
                                        
                                        ] as [String : Any?]
                                   
                                    
                                    
                                    
                                    let url1 = "http://login.academiamir.com/amir-central/api/events/"+self.clase.flavorCode!+"-"
                                    let url = url1+String(self.clase.eventId!)+"/confirmParticipation"
                                    print(parameters)
                                    
                                    Alamofire.request(url, method: .post, parameters: financials as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseData) -> Void in
                                        if((responseData.result.value) != nil) {
                                            self.timer.invalidate()
                                            self.timerIsOn = false
                                            print("Lista pasada")
                                            
                                        }}
                                    
                                    
                                    
                                    self.mostrarAlertaAulaEncontrada(title: "Aula encontrada", message: "Serás redirigido a la pantalla principal.\nGracias por asistir.")
                                    
                                }
                                
                            }
                                
                                
                                
                                
                            }
                        
                        
                        
                    }
                    
                }
                    
                
                
            }
            blueberryZone.onExit = { zoneContext in
                print("Exited near range of tag 'AULA'. Attachment payload: \(zoneContext.attachments)")
            }
            
            blueberryZone.onContextChange = { contexts in
                print("Now in range of \(contexts.count) contexts")
                
            }
        
            
        
            // Start proximity observation
            self.proximityObserver.startObserving([blueberryZone])
        
        
        
        if !timerIsOn {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,      selector: #selector(timerRunning), userInfo: nil, repeats: true)
            //3
            timerIsOn = true
        }
        
        
    }
    @objc func timerRunning() {
        self.contador = self.contador+1
        self.contadorMenos = self.contadorMenos-1
        self.label2Info.text = String(contadorMenos)+"s"
        if self.contador == 15{
            self.timer.invalidate()
            timerIsOn = false
            print("Despues del timer")
            print(clase.fullAddress!)
            self.labelInfo.text = "Buscando por GPS"
            
            
            let request = MKDirections.Request()
            request.source = MKMapItem.forCurrentLocation()
            
            
            let center = CLLocationCoordinate2D(latitude: clase.latitude!, longitude: clase.longitude!)
            let place = MKPlacemark(coordinate: center)
            let destino = MKMapItem(placemark: place)
            
            request.destination = destino
            
            request.requestsAlternateRoutes = false
            
            let directions = MKDirections(request: request)
            
            directions.calculate(completionHandler: {(response, error) in
                
                if error != nil {
                    print("Error getting directions")
                } else {
                    self.showRoute(response!)
                }
            })
        }
        
    }
    func showRoute(_ response: MKDirections.Response) {
        
        for route in response.routes {
            var distancia : Double
            let inicio = 0.0
            distancia = inicio.distance(to: route.distance)
            
            print(distancia)
            let distanciaTOPE = 500.0
            if distancia<distanciaTOPE{
                
                print("En clase fisica")
                self.labelInfo.text = "Aula encontrada"
                
                let token = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
                
                let headers = ["Authorization": "Basic \(token)",
                    "Content-Type": "application/x-www-form-urlencoded"]
                /*let parameters = [
                 "grant_type": "password",
                 "username": usuario,
                 "password": contra
                 ]*/
                let parameters = [
                    "grant_type": "password",
                    "username": self.defaults.string(forKey: "usuario" )!,
                    "password": self.defaults.string(forKey: "password" )!
                ]
                Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
                    if((responseData.result.value) != nil) {
                        let swiftyJsonVar = JSON(responseData.result.value!)
                        print(swiftyJsonVar)
                        //print(swiftyJsonVar["access_token"])
                        if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                            self.tokenAccesoUsuario = tokenDeAcceso
                            
                            self.defaults.set(tokenDeAcceso, forKey: "tokenUser")
                            self.defaults.set(true, forKey: "HasAppBeenOpenedBefore")
                            print (tokenDeAcceso)
                            print(self.tokenAccesoUsuario)
                            
                            
                            
                            
                            let headers = ["content-type": "application/json",
                                           "Authorization": "Bearer \(self.tokenAccesoUsuario)"]
                            
                            let financials = [
                                
                                "confirmTime": nil,
                                "classCode": nil
                                
                                ] as [String : Any?]
                            
                            
                            let url1 = "http://login.academiamir.com/amir-central/api/events/"+self.clase.flavorCode!+"-"
                            let url = url1+String(self.clase.eventId!)+"/confirmParticipation"
                            print(parameters)
                            
                            Alamofire.request(url, method: .post, parameters: financials as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseData) -> Void in
                                if((responseData.result.value) != nil) {
                                    self.timer.invalidate()
                                    self.timerIsOn = false
                                    print("Lista pasada")
                                    
                                }}
                            
                            
                            
                            self.mostrarAlertaAulaEncontrada(title: "Aula encontrada", message: "Serás redirigido a la pantalla principal.\nGracias por asistir.")
                            
                        }
                        
                    }
                    
                    
                    
                    
                }
                
                
                self.mostrarAlertaAulaEncontrada(title: "Aula encontrada", message: "Serás redirigido a la pantalla principal.\nGracias por asistir.")
            }else{
                self.mostrarAlertaAulaEncontrada(title: "Aula NO encontrada", message: "Sentimos que no te encuentres cerca de nuestras oficinas.\nGracias.")
            }
            
            
            
        }
    }
}
extension Request {
    
    public func debugLog() -> Self {
        
        #if DEBUG
        
        debugPrint("=======================================")
        
        debugPrint(self)
        
        debugPrint("=======================================")
        
        #endif
        
        return self
        
    }
    
}
