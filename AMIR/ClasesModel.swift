//
//  ClasesModel.swift
//  AMIR
//
//  Created by Ismael Catala on 3/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import Foundation
class Clase: NSObject {
    var flavorCode: String?
    var eventId: Int?
    var startDate: String?
    var endDate: String?
    var beacon: String?
    var sala: String?
    var latitude: Double?
    var longitude: Double?
    var fullAddress: String?
    init(flavorCode: String, eventId: Int, startDate: String, endDate: String, beacon: String, sala: String, latitude: Double, longitude: Double, fullAddress: String) {
        self.flavorCode = flavorCode
        self.eventId = eventId
        self.startDate = startDate
        self.endDate = endDate
        self.beacon = beacon
        self.sala = sala
        self.latitude = latitude
         self.longitude = longitude
        self.fullAddress = fullAddress
        super.init()
    }
}
