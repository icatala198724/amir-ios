//
//  Slide.swift
//  AMIR
//
//  Created by Ismael Catala on 3/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit

class Slide: UIView {
    let defaults:UserDefaults = UserDefaults.standard

    @IBOutlet weak var imagenView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    @IBOutlet weak var btnEmpezar: UIButton!
    
    @IBAction func btnEmpezarPress(_ sender: Any) {
        self.defaults.set(true, forKey: "onBoardSee")
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
