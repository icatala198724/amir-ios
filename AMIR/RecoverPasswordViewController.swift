//
//  RecoverPasswordViewController.swift
//  AMIR
//
//  Created by Ismael Catala on 6/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RecoverPasswordViewController: UIViewController {

    @IBOutlet weak var mailTextFieldRecover: UITextField!
    @IBAction func volverButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func RecoverPasswordButtonPressed(_ sender: Any) {
        if (self.mailTextFieldRecover.text != ""){
            var usuario : String = ""
            
            
            
            usuario = mailTextFieldRecover.text!
            
            let sv = UIViewController.displaySpinner(onView: self.view)
            let token2 = "aW9vbi1ldmVudHM6czBFMl5vMXlmJml1"
            let headers2 = ["Authorization": "Basic \(token2)",
                "Content-Type": "application/x-www-form-urlencoded"]
            let parameters2 = [
                "grant_type": "client_credentials",
                
                
                ]
            Alamofire.request("http://login.academiamir.com/amir-central/oauth/token", method: .post, parameters: parameters2, headers: headers2).responseJSON { (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    
                    if let tokenDeAcceso = swiftyJsonVar["access_token"].string{
                        
                        //UIViewController.removeSpinner(spinner: sv)
                        let headers = ["Authorization": "Bearer \(tokenDeAcceso)",
                            "Content-Type": "application/x-www-form-urlencoded"]
                        let parameters = [
                            
                            "email": usuario,
                            
                            ]
                        Alamofire.request("http://login.academiamir.com/amir-central/api/account/passwordRecoveryRequest", method: .post, parameters: parameters, headers: headers).responseJSON { (responseData) -> Void in
                            if((responseData.result.value) != nil) {
                                let swiftyJsonVar = JSON(responseData.result.value!)
                                print(swiftyJsonVar)
                                //print(swiftyJsonVar["access_token"])
                                UIViewController.removeSpinner(spinner: sv)
                                self.mostrarAlerta(title: "Sistema", message: "Se ha enviado una contraseña nueva a su email")
                                
                            }
                        }
                        
                    }
                    else{
                        UIViewController.removeSpinner(spinner: sv)
                        self.mostrarAlerta(title: "Error de acceso", message: "Usuario o contraseña incorrectos")
                    }
                    
                }
            }
            
            
            
        }else{
            mostrarAlerta(title: "Error de datos", message: "Se necesitan el correo")
        }
        
        
        print("Acceder pressed")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action:    #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    @objc func tapGestureHandler() {
        mailTextFieldRecover.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func mostrarAlerta(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
            
        })
        alertaGuia.addAction(aceptar)
        present(alertaGuia, animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
