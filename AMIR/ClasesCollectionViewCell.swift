//
//  ClasesCollectionViewCell.swift
//  AMIR
//
//  Created by Ismael Catala on 3/7/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit

class ClasesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ClaseLabel: UILabel!
    
    @IBOutlet weak var AulaLabel: UILabel!
    @IBOutlet weak var HoraLabel: UILabel!
}
