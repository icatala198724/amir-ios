//
//  ClaseCellTableViewCell.swift
//  AMIR
//
//  Created by Ismael Catala on 18/9/18.
//  Copyright © 2018 Ismael Catala. All rights reserved.
//

import UIKit

class ClaseCellTableViewCell: UITableViewCell {

    @IBOutlet weak var clasesLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var codigoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
